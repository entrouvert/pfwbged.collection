# -*- coding: utf-8 -*-
"""Installer for the pfwbged.collection package."""

from setuptools import find_packages
from setuptools import setup


long_description = (
    open('README.rst').read()
    + '\n' +
    'Contributors\n'
    '============\n'
    + '\n' +
    open('CONTRIBUTORS.rst').read()
    + '\n' +
    open('CHANGES.rst').read()
    + '\n')


setup(
    name='pfwbged.collection',
    version='1.0',
    description="Collections for PFWB GED project",
    long_description=long_description,
    # Get more from http://pypi.python.org/pypi?%3Aaction=list_classifiers
    classifiers=[
        "Environment :: Web Environment",
        "Framework :: Plone",
        "Framework :: Plone :: 4.2",
        "Framework :: Plone :: 4.3",
        "Programming Language :: Python",
        "Programming Language :: Python :: 2.7",
    ],
    keywords='pfwb,ged,collection',
    author='Frédéric Péters',
    author_email='fpeters@entrouvert.com',
    url='http://pypi.python.org/pypi/pfwbged.collection',
    license='GPL',
    packages=find_packages('src', exclude=['ez_setup']),
    namespace_packages=['pfwbged'],
    package_dir={'': 'src'},
    include_package_data=True,
    zip_safe=False,
    install_requires=[
        'setuptools',
        'five.grok',
        'plone.app.contenttypes',
        'collective.taskqueue[redis]',
    ],
    entry_points="""
    [z3c.autoinclude.plugin]
    target = plone
    """,
)
