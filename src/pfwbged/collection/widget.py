# -*- coding: utf8 -*-
from collective.dms.basecontent.relateddocs import IRelatedDocs
from collective.dms.basecontent.relateddocs import RelatedDocsWidget
from pfwbged.collection import _
from pfwbged.policy.interfaces import IPfwbgedPolicyLayer
from plone import api
from z3c.form.interfaces import IFieldWidget
from z3c.form.widget import FieldWidget
from zope.component import adapter
from zope.i18n import translate
from zope.interface import implementer


@adapter(IRelatedDocs, IPfwbgedPolicyLayer)
@implementer(IFieldWidget)
def PFWBRelatedDocsFieldWidget(field, request):
    return FieldWidget(field, PFWBRelatedDocsWidget(field.display_backrefs, request))


class PFWBRelatedDocsWidget(RelatedDocsWidget):

    def js_extra(self):
        return """\
                $('#%(id)s-widgets-query').each(function() {
                    if($(this).siblings('a.pfwbSearchButton').length > 0) { return; }
                    
                    $(document.createElement('a'))
                        .attr({
                            'href': '%(search_url)s'
                        })
                        .html('%(anchor_text)s')
                        .addClass('pfwbSearchButton')
                        .prepOverlay({
                            subtype: 'iframe',
                            config: {
                                onBeforeClose : function (e) {
                                    var selectedRows = this.getOverlay().find("iframe").contents().find("tr.selected");
                                    var input_box = $('#%(id)s-widgets-query');
                                    selectedRows.each(function(index, row) {
                                        var path = $(row).find(".colour-column input").data("value");
                                        var title = $(row).find(".title-column a").html();
                                        formwidget_autocomplete_new_value(input_box, path, title);
                                    });
                                    return true;
                                }
                            }
                        })
                        .insertAfter($(this));
                        
                    $(this).after(" ");
                });
                        
        """ % dict(search_url=api.portal.get().absolute_url() + '/pfwbsearch?iframe=1',
                   id=self.name.replace('.', '-'),
                   anchor_text=translate(_(u'Add'), context=self.request))
