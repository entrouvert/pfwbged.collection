
function store_history_parameter(name, value) {
    var next_state = history.state || {};
    next_state[name] = value;
    history.replaceState(next_state, document.title);
}

function get_history_parameter(name, default_value) {
  var history_state = history.state;
  if (history_state !== null) {
    if (history_state[name] !== undefined) {
        return history_state[name];
    } else {
        return default_value;
    }
  } else {
      return default_value;
  }
}

(function ($) {

    $.querywidget.updateSearch = function (stay_on_page) {
        var context_url = (function() {
            var baseUrl, pieces;
            baseUrl = $('base').attr('href');
            if (!baseUrl) {
                pieces = window.location.href.split('/');
                pieces.pop();
                baseUrl = pieces.join('/');
            }
            return baseUrl;
        })();
        var query = context_url + "/@@querybuilder_html_results?";
        var querylist  = [];
        var items = $('.QueryWidget .queryindex');
        if (!items.length) {
            return;
        }

        $('#orig-query-render select[name="form.widgets.query.i:records"]').each(function(i, elem) {
          /* when the querywidget is being initialized it will convert static
           * html elements to various widgets then call updateSearch, we take
           * the opportunity to handle those new widgets to move them around.
           */
          var index = $(elem).find('option:selected').val();
          if (index == 'portal_type') {
            return; /* already handled before */
          }

          var criteria_div = $('option[value="' + index + '"]:selected').parents('select').parent().detach();
          criteria_div.appendTo('.more-fieldset .criteria-content');
          criteria_div.find('select.queryindex').prop('disabled', true);
          criteria_div.find('.multipleSelectionWidget dt').removeClass('hiddenStructure');
          criteria_div.find('.multipleSelectionWidget dd').addClass('widgetPulldownMenu').addClass('hiddenStructure');
          var val = $(criteria_div).find('.queryvalue').val();

          if (index == 'thesaurus_keywords') {
             widget = $.querywidget.createWidget('StringWidget',
                     'thesaurus_keywords', 'form.widgets.query');
             $(widget).find('.queryvalue').val(val);
             $(widget).find('.querylabel').val(val);
             criteria_div.find('.queryvalue').replaceWith(widget);
          }
          if (index == 'Title') {
             $('input[name="text-criteria-title"]').val(val);
             $(criteria_div).hide();
          }
          if (index == 'SearchableText') {
             $('input[name="text-criteria-fulltext"]').val(val);
             $(criteria_div).hide();
          }
          if (val && (index == 'Title' || index == 'SearchableText')) {
             $('fieldset.text-fieldset div.criteria-content').show();
             $('fieldset.text-fieldset legend').addClass('expanded');
          }
        });

        items.each(function () {
            var results = $(this).parents('.criteria').children('.queryresults');
            var index = $(this).val();
            var operator = $(this).parents('.criteria').children('.queryoperator').val();
            var widget = $.querywidget.config.indexes[index].operators[operator].widget;
            querylist.push('query.i:records=' + index);
            querylist.push('query.o:records=' + operator);
            switch (widget) {
                case 'DateRangeWidget':
                    var daterangewidget = $(this).parents('.criteria').find('.querywidget');
                    querylist.push('query.v:records:list=' + $(daterangewidget.children('input')[0]).val());
                    querylist.push('query.v:records:list=' + $(daterangewidget.children('input')[1]).val());
                    break;
                case 'MultipleSelectionWidget':
                    var multipleselectionwidget = $(this).parents('.criteria').find('.querywidget');
                    multipleselectionwidget.find('input:checked').each(function () {
                        querylist.push('query.v:records:list=' + $(this).val());
                    });
                    break;
                default:
                    querylist.push('query.v:records=' + $(this).parents('.criteria').find('.queryvalue').val());
                    break;
            }

            $.get(context_url + '/@@querybuildernumberofresults?' + querylist.join('&'),
                  {},
                  function (data) { results.html(data); });
        });
        /*if (stay_on_page) {
          querylist.push($('a.batch-link.current').data('query'));
        }*/
        query += querylist.join('&');
        query += '&sort_on=' + $('#sort_on').val();
        if ($('#sort_order:checked').length > 0) {
            query += '&sort_order=reverse';
        }
        return $.querywidget.runQuery(query);
    };

    $.querywidget.runQuery = function (query) {
        orig_query = query;

        var batch_start = get_history_parameter('batch_start', '');
        if (batch_start) {
            query += '&' + batch_start;
        }

        if ($('#table-batchSize').val()) {
            query += '&table-batchSize=' + $('#table-batchSize').val();
        }

        var query_extra_columns = '';
        if ($('#table-extra-columns').length) {
          query_extra_columns = $('#table-extra-columns').val();
        } else if ($('#orig-extra-columns').val()) {
          query_extra_columns = $('#orig-extra-columns').val();
        }
        $('#form-extra-columns').val(query_extra_columns);
        if (query_extra_columns) {
            query += '&table-extra-columns=' + query_extra_columns;
        }
        $.get(query, {}, function (data) {
            $('#searchresults').html(data);
            edmlisting.initoverlay();
            $.querywidget.updateBatchLinks(orig_query, query_extra_columns);
            $.querywidget.updateBatchSizeMenu();
            $.querywidget.updateExtraColumnsMenu();
            $.querywidget.updateTableHeaders();
            $.querywidget.updatePreviewLinks();
            $.querywidget.updateMultiCheckboxes();
            $.pfwbged.prepareLinearNavigation();
        });
    };
    $.querywidget.updateBatchLinks = function (query, query_extra_columns) {
        $('a.batch-link').click(function() {
           var own_query = query + '&' + $(this).data('query');
           store_history_parameter('batch_start', $(this).data('query'));
           if (query_extra_columns) {
               own_query += '&table-extra-columns=' + query_extra_columns;
           }
           $.get(own_query, {}, function (data) {
             $('#searchresults').html(data);
             edmlisting.initoverlay();
             $.querywidget.updateBatchLinks(query, query_extra_columns);
             $.querywidget.updateBatchSizeMenu();
             $.querywidget.updateExtraColumnsMenu();
             $.querywidget.updateTableHeaders();
             $.querywidget.updatePreviewLinks();
            $.querywidget.updateMultiCheckboxes();
             $.pfwbged.prepareLinearNavigation();
           });
           return false;
        });
    };
    $.querywidget.updateBatchSizeMenu = function () {
        $('#batchsizes li').click(function () {
           var current_batch_size = $('#table-batchSize').val();
           var new_batch_size = $(this).text();
           $('#table-batchSize').val(new_batch_size);
           if (new_batch_size != current_batch_size) {
               $.querywidget.updateSearch();
           }
        });
        $('#batchsize-menu').click(function () {
            $('#batchsizes').toggle();
            return false;
        });
    };
    $.querywidget.updateExtraColumnsMenu = function () {
        col_names = $('#table-extra-columns').val();
        if (col_names == undefined) { return; }
        cols = col_names.split(':');
        $(cols).each(function(i, e) {
            if (e) {
              $('#extra-columns li input[value="' + e + '"]').click();
            }
        });
        $('#extra-columns li input').change(function () {
            var t = '';
            $('#extra-columns li input:checked').each(function(i, e) { t = t + $(e).val() + ':'; });
            $('#table-extra-columns').val(t);
        });
        $('#extra-columns li button').click(function () {
            $.querywidget.updateSearch();
            return false;
        });
        $('#extra-columns-menu').click(function () {
            $('#extra-columns').toggle();
            if ($('#extra-columns').css('display') == 'none') {
              /* update search when menu is closed */
              $.querywidget.updateSearch();
            }
            return false;
        });
    };
    $.querywidget.updateTableHeaders = function (query) {
        $('#searchresults table th span[data-sortable]').each(function(i, elem) {
            var sort_on = $(elem).data('sortable');
            $(elem).parent().css('cursor', 'pointer').click(function() {
                if ($('#sort_on').val() == sort_on) {
                    /* same column, reverse */
                    $('#sort_order').click();
                } else {
                    $('#sort_order').prop('checked', '');
                    $('#sort_on').val(sort_on);
                }
                store_history_parameter('sort_on', sort_on);
                store_history_parameter('sort_reverse', $('#sort_order').prop('checked'));
                $.querywidget.updateSearch();
            });
        });
    }

    $.querywidget.createWidgetOrig = $.querywidget.createWidget;
    $.querywidget.createWidget = function (type, index, fname) {
        result = $.querywidget.createWidgetOrig(type, index, fname);
        if (index == 'object_folders') {
            var span = $('<span>');
            var initial_input = result;
            $(result).css('display', 'none').appendTo(span);
            var input = $('<input type="text" readonly="readonly"/>');
            var button = $('<button>...</button>');
            $(input).appendTo(span);
            $(input).click(function() { $(button).click(); });
            $(button).click(function() {
                var window = $('<div class="contenttreeWindow"><div class="contenttreeWindowHeader"></div>'+
                           '<div class="contenttreeWidget"></div>' +
                           '<div class="contenttreeWindowActions">' +
                           '<input class="context contentTreeAdd" type="button" value="Valider" />' +
                           '<input class="standalone contentTreeCancel" type="button" value="Annuler" />' +
                           '</div></div>');
                var parent = $(input);
                window.showDialog('@@foldertree-fetch', 200);
                $(window).find('.contenttreeWidget').contentTree(
                    {
                        script: '@@foldertree-fetch',
                        folderEvent: 'click',
                        selectEvent: 'click',
                        expandSpeed: 200,
                        collapseSpeed: 200,
                        multiFolder: true,
                        multiSelect: false,
                        rootUrl: '/'
                    },
                    function(event, selected, data, title) {
                        // alert(event + ', ' + selected + ', ' + data + ', ' + title);
                    });
                $(window).find('.contentTreeAdd').unbind('click').click(function() {
                    var contenttree_window = $(this).parents(".contenttreeWindow");
                    contenttree_window.find('.navTreeCurrentItem > a').each(function () {
                      var folder_intid = $(this).data('intid');
                      var folder_label = $.trim($(this).text());
                      $(initial_input).val(folder_intid);
                      $(input).val(folder_label);
                      var multi = get_history_parameter('multi', {});
                      multi[index].value = folder_intid;
                      multi[index].label = folder_label;
                      store_history_parameter('multi', multi);
                    });
                    $(this).contentTreeCancel();
                    $.querywidget.updateSearch();
                });
                $(window).find('.contentTreeCancel').unbind('click').click(function() {
                    $(this).contentTreeCancel();
                });
                return false;
            });

            $(button).appendTo(span);
            result = span;
        }

        if (index == 'thesaurus_keywords') {
            var span = $('<span>');
            var initial_input = result;
            $(result).css('display', 'none').appendTo(span);
            var input = $('<input type="text" class="querylabel" readonly="readonly"/>');
            var button = $('<a href="#" style="display: none">...</a>');
            $(input).appendTo(span);
            $(input).click(function() { $(button).click(); });
            $(button).prepOverlay({
                              subtype: 'ajax',
                              filter: '#content>*',
                              urlmatch: '.*',
                              urlreplace: 'thesaurus'
                            });
            $(button).appendTo(span);
            result = span;
        }


        return result;
    };

    $.querywidget.updatePreviewLinks = function () {
          var xhr_preview = null;
          $('.ResultsTasksTable tr a, .ResultsDocumentsTable tr a, .ResultsTable tr a').hover(function() {
             $('#preview-doc').remove();
             if (xhr_preview !== null) xhr_preview.abort();
             xhr_preview = $.getJSON($(this).attr('href') + '/@@preview',
                function (data) {
                   xhr_preview = null;
                   $('#preview-doc').remove();
                   if (data.thumbnail_url) {
                     $('body').append('<div id="preview-doc">');
                     $('#preview-doc').append('<img src="' + data.thumbnail_url + '"/>');
                   }
             });
          }, function() {
             if (xhr_preview !== null) xhr_preview.abort();
             $('#preview-doc').remove();
          });
    };

    $.querywidget.updateMultiCheckboxes = function () {
      $('th span.colour-column-head').click(function(e) {
        var $table = $(this).parents('table');
        $table.find('td.colour-column input').click();
      });
      $('td.colour-column input').change(function(e) {
        if ($(this).prop('checked')) {
          $(this).parents('tr').addClass('selected');
        } else {
          $(this).parents('tr').removeClass('selected');
        }
        $('.multi-actions button').each(function(idx, elem) {
          var action_status = $(elem).data('status');
          var action_type = $(elem).data('type');
          var selector = 'tr.selected.row-state-' + action_status;
          if (action_type) {
            selector = selector + '.row-type-' + action_type;
          }
          if ($(this).parents('div.table').find(selector).length) {
            $(elem).show();
          } else {
            $(elem).hide();
          }
        });
        e.stopPropagation();
        return true;
      });
      $('.multi-actions button').on('click', function(ev) {
        var $div = $(this).parent();
        var url = $(this).parent().data('actions-url');
        var action = $(this).data('action');
        var action_status = $(this).data('status');
        var popup = $(this).data('popup');
        var documents = Array();
        $(this).parents('div.table').find('tr.selected.row-state-' + action_status + ' input').each(function(idx, elem) {
          documents.push($(elem).data('value'));
        });
        if (action) {
          $div.find('button').prop('disabled', true);
          $.post(url, {'action': action, 'documents': documents }
              ).done(function() {
                console.log('success');
              }).fail(function(data) {
                console.log('fail');
                alert(data.responseText);
              }).always(function() {
                $.querywidget.updateSearch(true);
                $div.find('button').prop('disabled', false);
              });
        }
        if (popup) {
          var base_popup_url = $(this).parent().data('popup-action-base-url');
          var $fake_a = $('<a class="' + popup + '" href="' + base_popup_url + '@@' + popup +
                          '?documents=' + documents.join(',') + '"></a>');
          $fake_a.appendTo('body');
          $fake_a.prepOverlay({
                  closeselector: "#form-buttons-cancel",
                  config: {closeOnClick: false, closeOnEsc: false},
                  filter: "#content>*:not(div.configlet),dl.portalMessage.error,dl.portalMessage.info",
                  formselector: "#form",
                  subtype: 'ajax',
                  noform: 'reload'
          });
          $fake_a.trigger(ev);
        }
      });
    }
}(jQuery));

function update_portaltypes()
{
   metatype = $('.metatypes input:checked').val();
   if ($('option[value="portal_type"]:selected').length == 0) {
     $('.addIndex option[value="portal_type"]').prop('selected', true);
     $('.addIndex').trigger('change');
   }
   chosen_types = $.map($('.subtype.'+metatype+' input[data-portaltype]:checked'), function(val, i) { return $(val).data('portaltype'); });
   all_types = $.map($('.subtype.'+metatype+' input[data-portaltype]'), function(val, i) { return $(val).data('portaltype'); });
   if (chosen_types.length == 0) {
     enabled_types = all_types;
   } else {
     enabled_types = chosen_types;
   }

   typelist = $('option[value="portal_type"]:selected').parents('select').next().next();
   typelist.find('input').prop('checked', false);
   $.each(enabled_types, function(index, value) {
     typelist.find('input[value="' + value + '"]').prop('checked', true);
   });
   $.querywidget.updateSearch();
}

function store_subtypes() {
    var metatype = $('.metatypes input:checked').val();
    var chosen_types = $.map(
        $('.subtype.'+metatype+' input[data-portaltype]:checked'),
        function(val, i) { return $(val).data('portaltype'); }
    );
    store_history_parameter('subtypes', chosen_types);
}

function select_metatype(elem)
{
  $('div.subtype').hide();
  $('option.subtype').hide();
  $(elem).addClass('selected');
  $('div.subtype.' + $(elem).val()).show();
  $('option.subtype.' + $(elem).val()).show();
  $('fieldset.subtype-fieldset div.criteria-content').show();
  $('fieldset.subtype-fieldset legend').addClass('expanded');
}

(function() {
  if ($('#criterias-form').length == 0 && $('#pfwbsearchform').length == 0) {
     return;
  }
  $('.portaltype-pfwbgedcollection #contentview-edit').hide();
  $('.portaltype-pfwbgedcollection #plone-document-byline').hide();
  $('.portaltype-pfwbgedcollection .QueryWidget').data('fieldname', 'form.widgets.query');

  /* sync with saved value */
  //$('input[type="hidden"][name="form.widgets.query.i:records"][value="portal_type"]').parent().parent().find(':checked').each();
  $('input[type="hidden"][name="form.widgets.query.i:records"]').each(function() {
     if ($(this).val() == 'portal_type') {
        $(this).parent().parent().find(':checked').each(function() {
           var portal_type = $(this).val();
           var checkbox = $('.subtype-fieldset').find('[data-portaltype="' + portal_type + '"]');
           if ($(checkbox).parents('.docs').length == 1) {
             $('.metatypes [value="docs"]').attr('checked', 'checked');
             select_metatype($('.metatypes [value="docs"]'));
           } else if ($(checkbox).parents('.tasks').length == 1) {
             $('.metatypes [value="tasks"]').attr('checked', 'checked');
             select_metatype($('.metatypes [value="tasks"]'));
           } else if ($(checkbox).parents('.contacts').length == 1) {
             $('.metatypes [value="contacts"]').attr('checked', 'checked');
             select_metatype($('.metatypes [value="contacts"]'));
           }
           $(checkbox).click();
        });
     } else if ($(this).val() == 'SearchableText') {
        /* use parameter when redirected from live search */
        var searchable_text_value = $(this).parent().parent().find('.queryvalue').val();
        $('input[name="text-criteria-fulltext"]').val(searchable_text_value);
        $(".criterias-fieldset.text-fieldset legend")
            .addClass('expanded')
            .parent().find('div.criteria-content').toggle('slide');
      }
  });

  $('option[data-index]').parents('select').change(function() {
   index = $(this).find('option[data-index]:selected').data('index');
   if ($('.addIndex option[value="' + index + '"]:selected').length == 0) {
     var multi = get_history_parameter('multi', {});
     multi[index] = {};
     store_history_parameter('multi', multi);
     console.log("option[data-index] change (L493), storing fresh multi index: " + index);
     $('.addIndex option[value="' + index + '"]').prop('selected', true);
     $('.addIndex').trigger('change');
     var criteria_div = $('#formfield-form-widgets-query div.criteria option[value="' + index + '"]:selected').parents('select').parent().detach();
     criteria_div.appendTo('.more-fieldset .criteria-content');
     criteria_div.find('select.queryindex').prop('disabled', true);
     $($(this).find('option[data-index]').parents('select').find('option')[0]).prop('selected', true);
   }
   return false;
  });


  /* install callbacks */

  $('.metatypes input').change(function() {
     store_history_parameter('metatype', $(this).attr("value"));
     select_metatype($(this));
     update_portaltypes();
     return false;
  });
  $('.subtype input').change(update_portaltypes);
  $('.subtype input').change(store_subtypes);

  $('input[name="text-criteria-title"]').on('keyup', function() {
    store_history_parameter('title', $(this).val());
    if ($('option[value="Title"]:selected').length == 0) {
      $('.addIndex option[value="Title"]').prop('selected', true);
      $('.addIndex').trigger('change');
    }
    $('option[value="Title"]:selected').parents('select').next().next().val($(this).val());
   $.querywidget.updateSearch();
  });

  $('input[name="text-criteria-fulltext"]').on('keyup', function() {
    store_history_parameter('fulltext', $(this).val());
    if ($('option[value="SearchableText"]:selected').length == 0) {
      $('.addIndex option[value="SearchableText"]').prop('selected', true);
      $('.addIndex').trigger('change');
    }
    $('option[value="SearchableText"]:selected').parents('select').next().next().val($(this).val());
   $.querywidget.updateSearch();
  });


  $('#formfield-form-widgets-query').hide();
  $('#formfield-form-widgets-sort_on').hide();
  $('#formfield-form-widgets-query').parent().show();

  if ($('#pfwbsearchform .error').length == 0) {
    $('#formfield-form-widgets-title').hide();
    $('#form-buttons-save').click(function() {
      $('#formfield-form-widgets-title').show();
      $(this).unbind('click');
      return false;
    });
  }
  $('.criterias-fieldset legend').click(function() {
    if ($(this).parent().find('div.criteria-content').is(':visible')) {
      $(this).removeClass('expanded');
    } else {
      $(this).addClass('expanded');
    }
    $(this).parent().find('div.criteria-content').toggle('slide');
  });

  $('.portaltype-pfwbgedcollection #plone-contentmenu-actions-save').click(function() {
    $('button[name="save"]').click();
    return false;
  });
  $('.portaltype-pfwbgedcollection #plone-contentmenu-actions-saveas').click(function() {
    $('#criterias-form').attr('action', $('#criterias-form').data('saveas'));
    $('#contentActionMenus .actionMenu .actionMenuHeader a')[0].click()
    $('.save-as-widgets').show();
    $('.save-as-widgets div').show();
    return false;
  });
  $('#pfwbged-criterias').parents('form').submit(function() {
    /* enable select (with criteria type) just before submit, so their values
     * are sent to the server.
     */
    $(this).find('select:disabled').enable();
  });

  $('#confirm-overlay').click(function() {
    $('div.close', window.parent.document).click();
  });
  $('#cancel-overlay').click(function() {
    $('#searchresults input:checkbox').removeAttr('checked');
    $('#searchresults tr').removeClass('selected');
    $('div.close', window.parent.document).click();
  });

  /* search restoration starts here */

  var restore_ppfwbged_search = function() {
    var history_state = history.state;
    if (history_state !== null) {
      if (history_state.metatype !== undefined) {
        var metatype = $('.metatypes [value="' + history_state.metatype + '"]');
        var subtypes = history_state.subtypes || [];
        $.each(subtypes, function(index, value) {
          $('input[data-portaltype="' + value + '"]').prop('checked', true);
        });
        metatype.prop('checked', true);
        metatype.trigger('change');  // refreshes metatype, then subtypes.

        var title = get_history_parameter('title', '');
        var fulltext = get_history_parameter('fulltext', '');
        if (title) {
            $('.addIndex option[value="Title"]').prop('selected', true);
            $('.addIndex').trigger('change');
            $('option[value="Title"]:selected').parents('select').next().next().val(title);
        }
        if (fulltext) {
            $('.addIndex option[value="SearchableText"]').prop('selected', true);
            $('.addIndex').trigger('change');
            $('option[value="SearchableText"]:selected').parents('select').next().next().val(fulltext);
        }
        if (title || fulltext) {
          $.querywidget.updateSearch();
          $('fieldset.text-fieldset div.criteria-content').show();
          $('fieldset.text-fieldset legend').addClass('expanded');
        }

        var multi = get_history_parameter('multi', {});
        var index_name;
        for (index_name in multi) {  // hack to emulate (.length > 0) for an object
          $('fieldset.more-fieldset div.criteria-content').show();
          $('fieldset.more-fieldset legend').addClass('expanded');
          break;
        }

        // create criteria line
        console.log("restore_ppfwbged_search, adding new criteria lines");
        for (index_name in multi) {
          if ($('.addIndex option[value="' + index_name + '"]:selected').length == 0) {
            console.log("restore_ppfwbged_search, adding new criteria line for " + index_name);
            $('.addIndex option[value="' + index_name + '"]').prop('selected', true);
            $('.addIndex').trigger('change');
            var criteria_div = $('#formfield-form-widgets-query div.criteria option[value="' + index_name + '"]:selected').parents('select').parent().detach();
            criteria_div.appendTo('.more-fieldset .criteria-content');
            criteria_div.find('select.queryindex').prop('disabled', true);
            $($(this).find('option[data-index]').parents('select').find('option')[0]).prop('selected', true);
          }
        }

        // set operator, if needed
        console.log("restore_ppfwbged_search, setting operators");
        for (index_name in multi) {
          var index_params = multi[index_name];
          if (index_params.hasOwnProperty("operator")) {
            var operator = $('option[value="' + index_name + '"]:selected').parents('select').parent().find(".queryoperator");
            if (operator.find("option:selected").val() != index_params.operator) {
              console.log("restore_ppfwbged_search, setting operator " + index_params.operator);
              operator.find("option[value='" + index_params.operator + "']").prop('selected', true);
              operator.trigger("change");
            }
          }
        }

        // set values, if needed
        console.log("restore_ppfwbged_search, setting values");
        for (index_name in multi) {
          var index_params = multi[index_name];
          if (index_params.hasOwnProperty("value")) {
            console.log("restore_ppfwbged_search, setting value for " + index_params.value.toString());
            var stored_value = index_params.value;
            var criteria = $('option[value="' + index_name + '"]:selected').parents('select').parent();
            var querywidget = criteria.find(".querywidget");
            var operator_value = criteria.find(".queryoperator").val();
            var widget_type = $.querywidget.config.indexes[index_name].operators[operator_value].widget;
            switch (widget_type) {
              case 'DateRangeWidget':
                $(querywidget.children('input')[0]).val(stored_value[0]);
                $(querywidget.children('input')[1]).val(stored_value[1]);
                break;
              case 'MultipleSelectionWidget':
                var selected_titles = [];
                $.each(stored_value, function(i, element_value) {
                    var input = querywidget.find('input[value="' + element_value + '"]');
                    input.prop('checked', true);
                    selected_titles.push(input.parent().children('span').html());
                });
                querywidget.find('.multipleSelectionWidgetTitle')
                    .attr('title', selected_titles.join(', '))
                    .html(selected_titles.join(', '));
                break;
              default:
                querywidget.val(stored_value);
                if (index_name === 'object_folders') {
                  querywidget.next("input").val(index_params.label);
                }
                break;
            }
            $.querywidget.updateSearch();
          }
        }

        var sort_on = get_history_parameter('sort_on');
        var sort_reverse = get_history_parameter('sort_reverse');
        if (sort_on !== undefined) {
            $("#sort_on").val(sort_on);
            $('#sort_order').prop('checked', '');
        }
        if (sort_reverse === true) {
            $('#sort_order').prop('checked', true);
        }
        if (sort_on !== undefined || sort_reverse === true) {
            $.querywidget.updateSearch();
        }
      }
    }
  };

  var waitForQuerywidget = setInterval(function () {
    if ($.querywidget.initialized_over === true) {
      restore_ppfwbged_search();
      clearInterval(waitForQuerywidget);
    }
  }, 500);

}(jQuery));
