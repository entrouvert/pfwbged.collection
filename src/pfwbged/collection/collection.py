from AccessControl import getSecurityManager
from zope.interface import Interface, implements
from five import grok
from zope import schema
from plone.directives import form
import z3c.form.interfaces
from z3c.form import form as z3c_form
from z3c.form import field
from Products.Five.browser.pagetemplatefile import ViewPageTemplateFile

from z3c.relationfield.schema import RelationChoice
from plone.formwidget.contenttree import ObjPathSourceBinder
from plone.dexterity.content import Container

from plone import api
from plone.supermodel import model

from .searchview import PfwbQueryStringFieldWidget

from pfwbged.collection import _


class ICollection(model.Schema):
    """ """

    query = schema.List(
            title=u'Search terms',
            value_type=schema.Dict(value_type=schema.Field(),
                key_type=schema.TextLine()),
            required=False,
            )

    extra_columns = schema.List(
            title=u'Extra columns',
            value_type=schema.TextLine(),
            required=False)

    sort_on = schema.TextLine(default=u'sortable_title')
    sort_reversed = schema.Bool(default=False)

    form.widget(query=PfwbQueryStringFieldWidget)


class Collection(Container):
    """ """
    implements(ICollection)


class ISaveForm(Interface):
    title = schema.TextLine(title=_(u'Title'), required=True)


class SaveForm(z3c_form.Form):
    fields = field.Fields(ISaveForm)
    ignoreContext = True
    template = ViewPageTemplateFile('templates/dummy.pt')


class View(form.DisplayForm):
    grok.context(ICollection)
    grok.require('zope2.View')
    search_view_template = ViewPageTemplateFile('templates/collection.pt')
    criterias_template = ViewPageTemplateFile('templates/criterias.pt')
    schema = ICollection

    def update(self):
        if not 'title' in self.fields:
            self.fields += field.Fields(ISaveForm)
        super(View, self).update()

    def updateWidgets(self):
        if not 'title' in self.fields:
            self.fields += field.Fields(ISaveForm)
        self.fields['title'].ignoreContext = True
        self.fields['title'].ignoreRequest = True
        form.DisplayForm.updateWidgets(self)

        if self.widgets['query'].value:
            for criteria in self.widgets['query'].value:
                for key, value in criteria.items():
                    if type(value) is list:
                        criteria[unicode(key, 'utf-8')] = [unicode(x, 'utf-8') for x in value]
                    else:
                        criteria[unicode(key, 'utf-8')] = unicode(value, 'utf-8')

        self.widgets['sort_on'].mode = z3c.form.interfaces.HIDDEN_MODE
        self.widgets['sort_reversed'].mode = z3c.form.interfaces.INPUT_MODE
        sm = getSecurityManager()
        self.can_modify = sm.checkPermission('Modify portal content',
                                              self.context)

    def search_uri(self):
        return api.portal.get().absolute_url() + '/pfwbsearch'

    def extra_columns(self):
        return ':'.join(self.context.extra_columns or [])

    def save_form(self):
        f = SaveForm(api.portal.get(), self.request)
        f.update()
        return f.render()

    def render(self):
        if self.can_modify and 'save' in self.request.form:
            self.save()
            self.request.response.redirect(self.context.absolute_url())
            return
        return self.search_view_template()

    def save(self):
        from plone.app.querystring.queryparser import parseFormquery
        self.context.query = parseFormquery(self.context, self.request.form.get('form.widgets.query'))
        self.context.query = []
        for criteria in self.request.form.get('form.widgets.query'):
            new_dict = {}
            self.context.query.append(new_dict)
            for key, value in criteria.items():
                if type(value) is list:
                    new_dict[unicode(key, 'utf-8')] = [unicode(x, 'utf-8') for x in value]
                else:
                    new_dict[unicode(key, 'utf-8')] = unicode(value, 'utf-8')
        self.context.extra_columns = [x for x in self.request.form.get('form-extra-columns', '').split(':') if x]
        self.context.sort_on = self.request.form.get('form.widgets.sort_on')
        self.context.sort_reversed = (self.request.form.get('form.widgets.sort_reversed') and
                'selected' in self.request.form.get('form.widgets.sort_reversed'))
