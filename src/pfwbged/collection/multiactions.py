from Products.Five.browser import BrowserView
from collective.taskqueue import taskqueue
from pfwbged.basecontent.behaviors import IPfwbIncomingMail
from plone import api


class BackgroundDocumentTransitionView(BrowserView):

    def __call__(self):
        document = self.context
        action = self.request.form['action']

        if action in ('validate', 'refuse'):
            for child in reversed(document.values()):
                if child.portal_type == 'dmsmainfile':
                    api.content.transition(child, action)
                    break
        else:
            api.content.transition(document, action)

        document.reindexObject(idxs=['review_state'])

        if action == 'to_process' and IPfwbIncomingMail.providedBy(document):
            from pfwbged.policy.subscribers.mail import incoming_mail_attributed
            incoming_mail_attributed(document, u'')


class MultiActionsView(BrowserView):
    def __call__(self):
        action = self.request.form['action']  # ex: process_without_comment
        documents = self.request.form['documents[]']
        if isinstance(documents, basestring):
            documents = [documents]

        for document_path in documents:
            taskqueue.add(
                '{}/background_document_transition'.format(document_path),
                params={"action": action}
            )

        return "OK"
